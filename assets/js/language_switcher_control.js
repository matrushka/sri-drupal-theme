(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.languageSwitcher = {
    attach: function (context, settings) {
      var $languageSwitcherToggle = $(".sri-language-button", context);
      var $languageSwitcherContainer = $(".language-switcher__container", context);

      if($languageSwitcherToggle.length > 0 && $languageSwitcherContainer.length > 0) {
  
        $languageSwitcherToggle.click(function(e){
          e.preventDefault();
          $languageSwitcherContainer.toggleClass('is-hidden');
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);