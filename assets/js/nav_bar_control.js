(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.navbarControl = {
    attach: function (context, settings) {
      var $headerWrapper = $(".header__wrapper", context);
      
      if($headerWrapper.length > 0) {
        // Intial scroll offes and header wrapper offset
        var initialScrollOffset = $(document).scrollTop();
        var headerWrapperOffset = $headerWrapper.height() + $headerWrapper.offset().top - initialScrollOffset;

        // Mobile navbar toggler process
        var isMobileMenuExpanded = false;
        var $navBarToggler = $headerWrapper.find('.navbar-toggler');
        var $navBarWrapper = $headerWrapper.find('.navbar__wrapper');
        $navBarToggler.click(function(e){
          e.preventDefault();
          $navBarWrapper.css('padding-top', ($headerWrapper.height() + $headerWrapper.position().top) + 'px');
          $navBarWrapper.toggleClass('is-expanded');
          isMobileMenuExpanded = $navBarWrapper.hasClass('is-expanded');
        });

        // Content table process
        var $contentTable = $(".content-table__table", context);
        var $pageTitle = $(".page-title", context);
        if($contentTable.length > 0 && $pageTitle.length > 0) {
          // Content table offset and trigger height
          var contentTableOffset = $contentTable.height() + $contentTable.offset().top;
          var triggerHeight = contentTableOffset - headerWrapperOffset;

          // Get list of anchor triggers
          var $contentList = $contentTable.find('a');
          var anchorList = $contentList.map(function(index, el){
            var anchorName = $(el).attr('href').substr(1);
            var $anchor = $('a[name='+ anchorName +']');
            var anchorOffset = $anchor.height() + $anchor.offset().top;
            var anchorTrigger = anchorOffset - headerWrapperOffset;
            return {
              name: anchorName,
              trigger: anchorTrigger
            };
          }).get();
          
          // Cloning contents
          var $contentTableContents = $contentTable.clone();
          var $pageTitleContents = $pageTitle.clone();
          $headerWrapper.find('.navigation__content-table').append($contentTableContents);
          $headerWrapper.find('.navigation__content-title').append($pageTitleContents);
          $contentTable.find('a').click(scrollToContent);
          $headerWrapper.find('a').click(scrollToContent);

          // Initializing control variables
          var lastIndexName = null;
          var latestScrollPosition = 0;
        }

        // Behavior functions
        function updateContentTableIndex(selectedAnchor) {
          var $currentAnchor = $headerWrapper.find('.navigation__content-table a[href="#'+ selectedAnchor +'"]');
          var $contentTableTable = $headerWrapper.find('.content-table__table');
          var horizontalOffset = $contentTableTable.find('a').first().offset().left;
          var currentLeftScroll = $currentAnchor.offset().left - horizontalOffset;
          $contentTableTable.find('a').removeClass('is-active')
          $currentAnchor.addClass('is-active');
          $contentTableTable.scrollLeft(currentLeftScroll);
        }

        function analizeScroll(currentScroll, goingUp) {
          if(currentScroll >= triggerHeight && !goingUp && !isMobileMenuExpanded) {
            $headerWrapper.addClass('is-shrinked');
          } else {
            $headerWrapper.removeClass('is-shrinked');
          }
          var selectedAnchor = null;
          $.map(anchorList, function(el, index){
            if(currentScroll >= el.trigger) {
              selectedAnchor = el.name;
            } else {
              return false;
            }
          });
          if(lastIndexName != selectedAnchor) {
            updateContentTableIndex(selectedAnchor);
          }
        }

        function scrollToContent(e) {
          // e.preventDefault();
          setTimeout(function(){
            var anchorName = $(e.currentTarget).attr('href').substr(1);
            var anchor = anchorList.find(function(el){
              return el.name == anchorName;
            });
            var scrollDistance = anchor.trigger - headerWrapperOffset;
            $(document).scrollTop(scrollDistance);
          }, 50);
          // document.location.hash = anchorName;
        }

        // Scroll event trigger
        $(document).scroll(function(e){
          var currentScroll = $(document).scrollTop();
          var goingUp = currentScroll <= latestScrollPosition;
          latestScrollPosition = currentScroll;
          analizeScroll(currentScroll, goingUp);
        });

      }
    }
  }
})(jQuery, Drupal, drupalSettings);