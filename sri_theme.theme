<?php


/**
 * @file
 * Functions to support theming in the sri_theme theme.
 */

/**
 * Implements hook_preprocess_HOOK() for html.html.twig.
 */
function sri_theme_preprocess_html(array &$variables) {
  /* Add class to html tag */
  //$variables['html_attributes']->addClass('no-js');

  // Don't display the site name twice on the front page (and potentially others)
  /*if (isset($variables['head_title_array']['title']) && isset($variables['head_title_array']['name']) && ($variables['head_title_array']['title'] == $variables['head_title_array']['name'])) {
    $variables['head_title'] = $variables['head_title_array']['name'];
  }*/
}

/**
 * Implements hook_page_attachments_alter().
 */
function sri_theme_page_attachments_alter(array &$page) {
  // Tell IE to use latest rendering engine (not to use compatibility mode).
  /*$ie_edge = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => [
    'http-equiv' => 'X-UA-Compatible',
    'content' => 'IE=edge',
    ],
  ];
  $page['#attached']['html_head'][] = [$ie_edge, 'ie_edge'];*/
}

/**
 * Implements hook_preprocess_page() for page.html.twig.
 */
function sri_theme_preprocess_page(array &$variables) {
  // Setting up is_brochure variable
  $variables['is_brochure'] = false;
  if( isset($variables['node']) ) {
    $node = $variables['node'];
    $node_bundle = $node->bundle();
    if($node_bundle == 'page') {
      $variables['is_brochure'] = true;
    }
  }
}

/**
 * Implements hook_theme_suggestions_page_alter().
 */
function sri_theme_theme_suggestions_page_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_theme_suggestions_node_alter().
 */
function sri_theme_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  $node = $variables['elements']['#node'];
  $node_bundle = $node->bundle();
  $view_mode = $variables['elements']['#view_mode'];

  if ($node_bundle == 'freeform' && $view_mode == 'full') {
    $code = $node->field_freeform_code->getString();
    if(!empty($code)) {
      $suggestions[] = 'node__freeform__' . $code;
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for Block document templates.
 */
function sri_theme_preprocess_block(array &$variables) {
}

/**
 * Implements hook_theme_suggestions_field_alter().
 */
function sri_theme_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  /*$element = $variables['element'];
  $suggestions[] = 'field__' . $element['#view_mode'];
  $suggestions[] = 'field__' . $element['#view_mode'] . '__' . $element['#field_name'];*/
}

/**
 * Implements hook_theme_suggestions_field_alter().
 */
function sri_theme_theme_suggestions_fieldset_alter(array &$suggestions, array $variables) {
  /*$element = $variables['element'];
  if (isset($element['#attributes']['class']) && in_array('form-composite', $element['#attributes']['class'])) {
    $suggestions[] = 'fieldset__form_composite';
  }*/
}

/**
 * Implements hook_preprocess_node().
 */
function sri_theme_preprocess_node(array &$variables) {
  $node = $variables['node'];
  $view_mode = $variables['view_mode'];

  if($node->bundle() == 'freeform' && $view_mode == 'full') {
    $paragraphs = $node->field_freeform_contents;
    $variables['freeform'] = [];

    foreach($paragraphs as $i => $paragraph_item) {
      $para = $paragraph_item->entity;
      $para_code = $para->field_freeform_code->value;

      $variables['freeform'][$para_code] = [
        'contents' => $variables['content']['field_freeform_contents'][$i],
        'entity' => $para
      ];
    }
  }

  if($node->bundle() == 'resource') {
    $image_field = @$variables['content']['field_featured_image'];
    $has_image_field = isset($image_field);

    if($has_image_field) {
      $variables['featured_image'] = array(
        'path' => $image_field[0]
      );
    }
  }

  if($node->bundle() == 'resource_collection') {
    $resources = $node->field_resources;
    $num_resources = $resources->count();
    $variables['resources_count'] = $num_resources;
  }

  if($node->bundle() == 'page') {
    // Forcing 'transpartent' as format value for the first section in pages
    $sections = $node->field_body;
    $first_section = $sections->first()->entity;
    $first_section->field_section_format->setValue('transparent');
  }
}

/**
 * Implements hook_theme_suggestions_views_view_alter().
 */
function sri_theme_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_preprocess_form().
 */
function sri_theme_preprocess_form(array &$variables) {
  //$variables['attributes']['novalidate'] = 'novalidate';
}

function sri_theme_preprocess_views_exposed_form(&$variables) {
  $form_id = $variables['form']['#id'];

  if($form_id =  'views-exposed-form-search-page') {
    _sri_theme_preprocess_search_page_exposed_form($variables);
  }
}

function _sri_theme_preprocess_search_page_exposed_form(&$variables) {
  // Getting form array as reference
  $form = &$variables['form'];

  if($form['#id'] == 'views-exposed-form-search-page') {
    // To follow the "I want to find" narrative, instead of - All - we change the
    // "All" option label to All content, so that it reads:
    // "I want to find All content"
    $variables['form']['content_type']['#options']['All'] = t('All content');

    // Esto ya lo hicimos, casi igual, en UPR. En el tema de UPR hay un montón de código de referencia.
    _sri_theme_search_form_create_containers($variables);
    _sri_theme_search_form_conditionals($variables);
    _sri_theme_search_form_adjust_layout($variables);
  }
}

function _sri_theme_search_form_create_containers(&$variables) {
  $form = &$variables['form'];
  $form['top'] = [
    '#type' => 'container',
    '#attributes' => [
      'class' => [
        'search__top'
      ]
    ],
  ];

  $form['middle'] = [
    '#type' => 'container',
    '#attributes' => [
      'class' => [
        'search__middle'
      ]
    ],
  ];

  $form['bottom'] = [
    '#type' => 'container',
    '#attributes' => [
      'class' => [
        'search__bottom'
      ]
    ],
  ];
}

function _sri_theme_search_form_adjust_layout(&$variables) {
  // OJO, acá vamos a necesitar una buena revisión de Accesibilidad, checar que el cambio de estado del formulario no rompa las cosas
  // Sugiero (dice Noel) hacer primero los estilos y luego hacer revisión de accesibilidad. Podría haber algún retrabajo, pero así no nos agobiamos con todo de una sola vez.

  $form = &$variables['form'];

  $form['top']['content_type'] = $form['content_type'];
  unset($form['content_type']);

  $form['top']['search_api_fulltext'] = $form['search_api_fulltext'];
  unset($form['search_api_fulltext']);

  $form['middle']['field_resource_type'] = $form['field_resource_type'];
  unset($form['field_resource_type']);

  $form['middle']['field_news_type'] = $form['field_news_type'];
  unset($form['field_news_type']);

  $form['middle']['field_topics'] = $form['field_topics'];
  unset($form['field_topics']);

  $form['middle']['search_api_language'] = $form['search_api_language'];
  unset($form['search_api_language']);

  $form['middle']['sort_by'] = $form['sort_by'];
  unset($form['sort_by']);

  $form['middle']['sort_order'] = $form['sort_order'];
  unset($form['sort_order']);

  $form['bottom']['actions'] = $form['actions'];
  unset($form['actions']);

}

function _sri_theme_search_form_conditionals(&$variables) {
  $form = &$variables['form'];

  $form['field_news_type']['#states'] = array(
    'visible' => array(
      'select[id="edit-content-type"]' => array('value' => 'news_item'),
    ),
  );

  $form['field_resource_type']['#states'] = array(
    'visible' => array(
      'select[id="edit-content-type"]' => array('value' => 'resource'),
    ),
  );

  $invisible_on_all = array(
    'invisible' => array(
      'select[id="edit-content-type"]' => array('value' => 'All')
    )
  );

  $form['field_topics']['#states'] = $invisible_on_all;
}

/**
 * Implements hook_preprocess_select().
 */
function sri_theme_preprocess_select(array &$variables) {
  //$variables['attributes']['class'][] = 'select-chosen';
}

/**
 * Implements hook_preprocess_field().
 */
function sri_theme_preprocess_field(array &$variables, $hook) {
  /*switch ($variables['element']['#field_name']) {
  }*/
}

/**
 * Implements hook_preprocess_details().
 */
function sri_theme_preprocess_details(array &$variables) {
  /*$variables['attributes']['class'][] = 'details';
  $variables['summary_attributes']['class'] = 'summary';*/
}

/**
 * Implements hook_theme_suggestions_details_alter().
 */
function sri_theme_theme_suggestions_details_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_preprocess_menu_local_task().
 */
function sri_theme_preprocess_menu_local_task(array &$variables) {
  //$variables['element']['#link']['url']->setOption('attributes', ['class'=>'rounded']);
}
