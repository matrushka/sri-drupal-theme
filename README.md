# SRI new website - Theme for drupal 8

Prerequisites
- Yarn
- sass compiler
- Make command enabled

### Building the theme for the first time
Execute the following command to install dependencies with yarn, export assets and build the product css files for the first time.

````
make build
````

### Updating changes in Sass files automatically
To start development mode, run the following command:

````
make dev
````

Dependencies will be updated first, then sass will be executed in dev recursively mode. The css products will not be minified while compiling in dev mode.